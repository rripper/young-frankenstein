#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();

    Board *board;

    Side our_side;
    Side other_side;

    int value(int x, int y);

    int getScore(Board *board, Side side);

    // int getScore(Move *move);

    int minimax(Board *board, int depth, Side side);
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
