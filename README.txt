Ryan Ripper
CS 2
Assignment Week 10 - Othello Part 2

I worked without a partner for the past two weeks for the Othello assignments.

When I first attempted to implement a basic heuristic to beat SimplePlayer, I just iterated through the board with all my potential moves and chose the move that will result in me having more stones on the board than the opponent.  This was not winning as often as I wanted it to. So instead, I gave each box on the board a value that would either tell my AI to take the box or not to take the box, based on an integer value I assigned to each box. Thus, it would take the best move of all the possible moves provided that some boxes were valued higher than others.  This methodology worked to beat SimplePlayer a majority of the time.

I went on to attempt to implement a minimax algorithm.  I had a very difficult time attempting to implement the algorithm.  In the end, I was able to make the player think a certain depth into the game but it did not pass the minimax test supplied to the students and it would even lose to SimplePlayer and get completely destroyed by ConstantTimePlayer and BetterPlayer.  However, when I had attempted to implement the minimax algorithm, I wrote a helper function that takes a board rather than a move, like how I had my heuristic earlier, and instead of scoring a move with a value, I scored the entire board by weighting each box with a certian value.  I would iterate through the board and find all my possible moves, as I did before.  And then I would make boards out of all those possible moves.  From those boards, I would find the total weight of the boxes I occupied and attempted to maximize that by iterating through each possible board and finding the one that resulted in the highest possible weighting and returned the move associated to that board.  With this ideology in place, I was able to beat SimplePlayer a majority of the time and also beat ConstantTimePlayer almost every single time.

This strategy of mine, scoring the total weight of the board rather than each single move, allows for a much higher chance of obtaining high importance boxes, like the corner ones, and avoiding the boxes adjacent to the corner ones.

If I had implemented my minimax algorithm correctly, I feel like I would have been able to beat BetterPlayer with also implementing a heuristic to judge which move was the best a certain depth into the game.

My strategy will possibly not work against an AI that looks much farther into the game, such as looking many steps ahead at possible moves that would result in a potential win.  However, if my AI goes against another one that bases its sole desicion making on a heuristic algorithm, I think it would have a good change in beating them.