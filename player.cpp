#include "player.h"
#include "board.h"
#include "common.h"
#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <fstream>

using namespace std;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side)
{
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     
    this->board = new Board();
     
    this->our_side = side;
     
    if (our_side == BLACK)
    {
		this->other_side = WHITE;
	}
	else
	{
		this->other_side = BLACK;
	}
}

/*
 * Destructor for the player.
 */
Player::~Player()
{
	
}

int Player::value(int x, int y)
{
	if ((x == 0 || x == 7) && (y == 0 || y == 7)) return 98;
	
	if ((x == 1 || x == 6) && (y == 0 || y == 7)) return -24;
	
	if ((x == 2 || x == 5) && (y == 0 || y == 7)) return 8;
	
	if ((x == 3 || x == 4) && (y == 0 || y == 7)) return 6;
	
	if ((x == 1 || x == 6) && (y == 1 || y == 6)) return -40;
	
	if ((x == 2 || x == 5) && (y == 1 || y == 6)) return -4;
	
	if ((x == 3 || x == 4) && (y == 1 || y == 6)) return -3;
	
	if ((x == 2 || x == 5) && (y == 2 || y == 5)) return 7;
	
	if ((x == 3 || x == 4) && (y == 2 || y == 5)) return 4;
	
	if ((x == 3 || x == 4) && (y == 3 || y == 4)) return 0;
	
	if ((x == 0 || x == 7) && (y == 1 || y == 6)) return -24;
	
	if ((x == 0 || x == 7) && (y == 2 || y == 5)) return 8;
	
	if ((x == 1 || x == 6) && (y == 2 || y == 5)) return -4;
	
	if ((x == 3 || x == 4) && (y == 3 || y == 4)) return 6;
	
	if ((x == 1 || x == 6) && (y == 3 || y == 4)) return -3;
	
	if ((x == 2 || x == 5) && (y == 3 || y == 4)) return 4;
	
	return 0;
}

int Player::getScore(Board *board, Side side)
{
	// HEURISTIC MODEL
	
	/*Board * temp_board = this->board->copy();
	temp_board->doMove(move, our_side);*/
	
	int score = 0;
	
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if (board->get(side, i, j))
			{
				score += value(i, j);
			}
		}
	}
	
	/*if (our_side == BLACK)
	{
		score = board->countBlack() - board->countWhite();
	}
	else
	{
		score = board->countWhite() - board->countBlack();
	}*/
	
	return score;
}

int Player::minimax(Board *board, int depth, Side side)
{
	int best_val;
	
	if (depth == 0 || not board->hasMoves(side))
	{
		return getScore(board, side);
	}
	
	//Board * current_board = board->copy();
	
	//Find next possible boards for the side opposite of input.
	
	Side side1;
	
	if (side == our_side)
	{
		side1 = other_side;
	}
	else
	{
		side1 = our_side;
	}
	
	Board * next_boards[60] = {NULL};
    
    int k = 0;
    
    for (int i = 0; i < 8; i++)
    {
		for (int j = 0; j < 8; j++)
		{
			Move * m = new Move(i, j);
			if (board->checkMove(m, side1))
			{
				Board * current_board = board->copy();
				current_board->doMove(m, side1);
				next_boards[k] = current_board;
				k++;
			}
		}
	}
	
	if (side == our_side)
	{
		best_val = -100000;
		for (int i = 0; i < k; i++)
		{
			if (getScore(next_boards[i], our_side) > best_val)
			{
				int val = minimax(next_boards[i], depth - 1, other_side);
				best_val = std::max(best_val, val);
			}
		}
	}
	else
	{
		best_val = 100000;
		for (int i = 0; i < k; i++)
		{
			if (getScore(next_boards[i], other_side) < best_val)
			{
				int val = minimax(next_boards[i], depth - 1, our_side);
				best_val = std::min(best_val, val);
			}
		}
	}
	
	return best_val;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft)
{
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
     
    if (opponentsMove != NULL)
    {
		this->board->doMove(opponentsMove, other_side);
	}
    
    Move * possible_moves[60] = {NULL};
    
    Board * possible_boards[60] = {NULL};
    
    int k = 0;
    
    for (int i = 0; i < 8; i++)
    {
		for (int j = 0; j < 8; j++)
		{
			Move * m = new Move(i, j);
			if (this->board->checkMove(m, our_side))
			{
				if ((m->getX() == 0 || m->getX() == 7) && (m->getY() == 0 || m->getY() == 7))
				{
					this->board->doMove(m, our_side);
					return m;
				}
				if ((m->getX() == 2 || m->getX() == 5) && (m->getY() == 0 || m->getY() == 7))
				{
					this->board->doMove(m, our_side);
					return m;
				}
				if ((m->getX() == 0 || m->getX() == 7) && (m->getY() == 2 || m->getY() == 5))
				{
					this->board->doMove(m, our_side);
					return m;
				}
				possible_moves[k] = m;
				k++;
			}
		}
	}
	
	if (k == 0)
	{
		std::cerr << "Returning null." << std::endl;
		return NULL;
	}
	
	for (int i = 0; i < k; i++)
	{
		Board * temp_board = this->board->copy();
		temp_board->doMove(possible_moves[i], our_side);
		possible_boards[i] = temp_board;
	}
	
	Move * best_move = possible_moves[0];
	int max_score = -64;
	
	for (int i = 0; i < k; i++)
	{
		int score = minimax(possible_boards[i], 0, our_side);
		if (score > max_score)
		{
			max_score = score;
			best_move = possible_moves[i]; // Need to set the best_move
		}
	}
	
	this->board->doMove(best_move, our_side);
		
	return best_move;	
}
